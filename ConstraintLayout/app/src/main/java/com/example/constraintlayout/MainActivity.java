package com.example.constraintlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;



public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void Send(View view) {
        Button send = findViewById(R.id.button);
        Button cancel = findViewById(R.id.button2);
        String stringName = send.getText().toString();
        String stringPhone = cancel.getText().toString();
        Intent i = new Intent(this, Send.class);
        i.putExtra("send", stringName);
        i.putExtra("cancel",stringPhone);
        startActivityForResult(i, REQUEST_CODE);
    }

    public void cancel(View view) {
        finishAffinity();
    }
}