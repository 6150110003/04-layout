package com.example.constraintlayout;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class Send extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        Intent i = getIntent();
        String inputString = i.getStringExtra("send");
        String yourPhone = i.getStringExtra("cancel");
        TextView view = (TextView) findViewById(R.id.tvShow);
        view.setText("Hi, " + inputString + "\nYour age is " + yourPhone.toString());
    }

    public void cancel(View view) {
        finishAffinity();
    }
}
